/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

/** Class Sources */
function Sources (classesWithLinks, hideLinks) {
    this.links = classesWithLinks;
    this.cssClasses =  [];
    this.usedSources = [];

    addQuotesToParagraphsInBlockquote();
    this.loadCSSClasses();
    this.addLinksToElements();
    this.addCommasAfterLinksInParagraphs();
    this.checkElementWithSourceClass();
    this.hideNotUsedSources(hideLinks);
}

Sources.prototype.loadCSSClasses = function() {
    for (var cssClass in this.links) {
        this.cssClasses.push(cssClass);
    }
}

Sources.prototype.addLinksToElements = function() {
    var link = '';
    for (var cssClass in this.links) {
        link = this.links[cssClass];
        $('[data-source*=' + cssClass + ']').each(function(){
            addSourceLinkToObject($(this), ' ' + link);
        });
    }
};

Sources.prototype.addCommasAfterLinksInParagraphs = function() {
    $('p > a.src:not(:last-child)').after(', ');
};

Sources.prototype.checkElementWithSourceClass = function() {
    var sources = this;
    $("[data-source]").each(function(){
        var element = $(this);
        var elementClasses = $(this).attr('data-source').split(/\s+/);
        $.each(elementClasses, function(index, value) {                    
            if (sources.isValidClass(value)) {
                sources.usedSources.push(value);
            } else {
                appendInvalidSource(element, value);
            }
        });
    });
};

Sources.prototype.isValidClass = function(className) {
    return $.inArray(className, this.cssClasses) > -1;
};

Sources.prototype.hideNotUsedSources = function(hideLinks) {
    if (hideLinks) {
        var sources = this;
        $.each(this.cssClasses, function(index, value) {      
            sources.hideLinkIfNotUsed(index, value);
        });
    }
};

Sources.prototype.hideLinkIfNotUsed = function(id, className) {
    if (!this.isUsedClass(className)) {
        $('aside.sources li[id=' + (id + 1) + ']').addClass('notUsed');
    }
}

Sources.prototype.isUsedClass = function(className) {
    return $.inArray(className, this.usedSources) > -1;
};


/** HELPER FUNCTIONS */
function addSourceLinkToObject(object, link) {
    var currentContent = object.html();
    object.html(appendSourceLink(currentContent, link));
}

function appendSourceLink(text, link) {
    var lastChar = text.substring(text.length - 1, text.length);
    if (isPunctuationChar(lastChar)) {
        var beforeLastChar = text.substring(0, text.length - 1);
        return beforeLastChar + link + lastChar;
    } else {
        return text + link;
    }
}

function isPunctuationChar(character) {
    var punctuationChars = ['.', ',', ':', ';'];
    return $.inArray(character, punctuationChars) > -1;
}

function appendInvalidSource($obj, title) {  
    $obj.append('<span class="invalidSource" title="' + title + '">[INVALID SOURCE]</span>');
}  

function addQuotesToParagraphsInBlockquote() {
    $('#main blockquote p').prepend('&bdquo;').append('&ldquo;');
}
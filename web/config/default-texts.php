<?php
# Header in root folder (name of the website/application)
define('TREEWEC_HEADER_MAINPAGE', 'Diplomová práce');
# ERROR Message - used in header if there was error 404,...
define('TREEWEC_HEADER_ERROR', 'Stránka nebyla nalezena');
# Breadcrumb navigation
define('TREEWEC_NAV_NEXT', 'NEXT');
define('TREEWEC_NAV_PREVIOUS', 'PREVIOUS');
define('TREEWEC_NAV_CONTENT', 'CONTENT');
define('TREEWEC_NAV_PARENT', 'PARENT');
?>
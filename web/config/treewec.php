<?php
# Treewec settings
define('TREEWEC_URL_REWRITE', true);
define('TREEWEC_WEB_URL', 'http://localhost/_treewec/Themes/TreeType/');
define('TREEWEC_ADMIN_PUBLIC_URL', 'http://localhost/_treewec/Themes/TreeType/public/treewec/');
define('TREEWEC_WEB_THEME', TREEWEC_WEB_THEMES . 'TreeType/');
define('TREEWEC_ADMIN_THEME', TREEWEC_WEB_THEMES . 'TreeType/');
define('TREEWEC_ADMIN_EDITOR', 'contenteditable');
define('TREEWEC_ADMIN_AUTHENTICATION', '\TreewecAuth\FreeAccess');
define('TREEWEC_ADMIN_LANGUAGE', 'czech');
# Default settings
define('TREEWEC_FILES_COMPARATOR', Treewec\Iterators\Directory\Order\ComparatorType::USER);
define('TREEWEC_FILES_ORDER', Treewec\Iterators\Directory\Order\OrderType::DESCENDING);
define('TREEWEC_FILES_DEPTH_IN_PAGES', 2);
# Files
define('TREEWEC_HIDDEN_FILES_PREFIX', '.');
define('TREEWEC_HIDDEN_FILES_BY_USER', '');
define('TREEWEC_FILES_EXTENSION', '.php');
define('TREEWEC_FILES_INDEX_NAME', 'index');
?>
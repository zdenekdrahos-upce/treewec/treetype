<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

// slash must be at the end
define('TREETYPE_NAME_OF_SOURCES_DIR', 'použité-zdroje/');

define('TREETYPE_VIEW_PATH', TREEWEC_WEB_THEME . 'view/');
?>

<p data-source="bp-drahos">Jak už bylo naznačeno<?php \TreeType\note('lorem', 'Cos to říkal? '); ?> v&nbsp;oddílu 4.4,tak ve vlastní aplikaci nejde přistupovat přímo ke konkrétnímu databázovému systému, ale k&nbsp;přístupu do databáze je třeba využít rozhraní. Databázové rozhraní IDatabase využívané v&nbsp;aplikaci je v&nbsp;příloze C. Podporované databáze toto rozhraní implementují, konkrétně se jedná o&nbsp;třídy MySQLDatabase a&nbsp;OracleDatabase. Kromě těchto tříd ke každé podporované databázi náleží také skript pro vytvoření databázových objektů a&nbsp;základní inicializaci pomocných tabulek. Skripty jsou v&nbsp;adresáři install a&nbsp;jsou využívány během instalace aplikace.</p>
<?php 
$array = array('Hello' => 'World');
foreach ($array as $key => $value) {
    echo "$key $value!";
}
?>

                    <figure>
                        <code>&lt;?php
$hello = 'Hello';
$world = 'World';
echo "{$hello} {$world}!";
?&gt;</code>
                    </figure>
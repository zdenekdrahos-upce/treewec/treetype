<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

$listPrefix = \TreeType\Elements\HeadingCounter::getPrefixForPage($pageInfo);
$captionPrefix = substr($listPrefix, 0, -1);
$level = count($pageInfo->pathInfo);
$h2Before = $level <= 2 ? "content: '{$captionPrefix}.' counter(sub-section)" : 'content: "";padding-right:0';
$h3Before = $level <= 1 ? "content: '{$captionPrefix}.' counter(sub-section) '.' counter(composite)" : 'content: "";padding-right:0';
?>
        <style type="text/css">
<?php if (!isset($_GET['pages'])): ?>
        #main h1:before { content: '<?php echo $captionPrefix; ?>'}
        #main h2:before {<?php echo $h2Before; ?>}
        #main h3:before {<?php echo $h3Before; ?>}
<?php else: ?>
        #main > div {counter-reset: section <?php echo TreeType\Elements\HeadingCounter::getValue($pageInfo, 1); ?>}
        #main h1 {counter-reset: sub-section <?php echo TreeType\Elements\HeadingCounter::getValue($pageInfo, 2); ?>}
        #main h2 {counter-reset: composite <?php echo TreeType\Elements\HeadingCounter::getValue($pageInfo, 3); ?>}
        #main h3 {counter-reset: detail <?php echo TreeType\Elements\HeadingCounter::getValue($pageInfo, 4); ?>}
<?php endif; ?>
        #main ol.content li:before { content: "<?php echo $listPrefix; ?>" counters(item, ".");}
        </style>

<?php
/*
 * $pageInfo
 * $pathEditFile
 * $pathEditFolder
 */
$pathEditFolder = $pageInfo->currentPath;
if ($pageInfo->pageType == 'FILE' && !in_array($pageInfo->header, array(TREEWEC_HEADER_MAINPAGE, TREEWEC_HEADER_ERROR)) && count($pageInfo->pathInfo) > 1) {
    $pathEditFolder = $pageInfo->pathInfo[count($pageInfo->pathInfo) - 2]->path;
}
$pathEditFile = isset($_GET['action']) && $_GET['action'] == 'todo' ? '.todo' : $pageInfo->currentPath;
?>

                <ul>
                    <li><a href="<?php echo TREEWEC_WEB_URL; ?>" title="Hlavní strana"><img src="<?php echo TREEWEC_THEME_PUBLIC_URL; ?>images/home.png" /></a></li>
                    <li>
                        <a href="<?php echo $pageInfo->currentPath; ?>">Aktuální strana</a>
                        <ul>
                            <li><a href="<?php echo $pathEditFolder; ?>&pages=all">Vše&nbsp;v&nbsp;jednom</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo $pageInfo->currentPath; ?>&action=indexes">Seznamy</a>
                        <ul>
                            <li><a href="<?php echo TREEWEC_WEB_URL; ?>&action=indexes&pages=all">Dokument</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo $pageInfo->currentPath; ?>&action=analysis">Analýzy</a>
                        <ul>
                            <li><a href="<?php echo TREEWEC_WEB_URL; ?>&action=analysis&pages=all">Dokument</a></li>
                        </ul>
                    </li>                    
                    <li><a href="<?php echo TREETYPE_NAME_OF_SOURCES_DIR; ?>">Zdroje</a></li>
                    <li><a href="&action=todo">TODO</a></li>
                    <li>
                        <a href="<?php echo $pageInfo->currentPath; ?>">Typografie</a>
                        <ul>
                            <li><a href="<?php echo (is_int(strpos($_SERVER['REQUEST_URI'], '/admin/')) ? './admin/' : '') . $pageInfo->currentPath; ?>&typography=show">Zobrazit&nbsp;po&nbsp;úpravě</a></li>
                        </ul>
                    </li>   
                    <li>
                        <a href="./admin/<?php echo $pathEditFile; ?>">Editovat</a>
                        <ul>
                            <li><a href="./admin/<?php echo $pageInfo->currentPath; ?>&action=rename">Přejmenovat</a></li>
                            <li><a href="./admin/<?php echo $pathEditFolder; ?>&action=add">Přidat&nbsp;stránku</a></li>
                            <li><a href="./admin/<?php echo $pathEditFolder; ?>&action=order">Změnit&nbsp;pořadí</a></li>
                            <li><a href="./admin/&admin=home">Admin&nbsp;home</a></li>
                        </ul>
                    </li>
                </ul>

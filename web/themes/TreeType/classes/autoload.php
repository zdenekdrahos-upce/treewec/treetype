<?php
/*
 * Autoload function for your classes
 */

function treeTypeAutoload($namespace) {
    if (strpos($namespace, 'TreeType\\') === 0) {
        $classPath = str_replace('\\', DIRECTORY_SEPARATOR, $namespace);
        require_once(__DIR__ . "/{$classPath}.php");
    } else {
        $html5 = __DIR__ . '/html5lib/' . $namespace . '.php';
        if (file_exists($html5)) {
            require_once($html5);
        }
    }
}

require_once(__DIR__ . '/TreeType/shorthands.php');

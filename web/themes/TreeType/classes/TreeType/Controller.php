<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType;

use Treewec\Iterators\Processors\Lists\ListFacade;
use TreeType\Iterators\TodoPrinter;
use TreeType\Iterators\ObsahPrinter;
use TreeType\Iterators\KnihaPrinter;

final class Controller
{
    /** @var \TreeType\View */
    private $view;
    /** @var \Treewec\Mvc\View */
    private $treewecView;
    /** @var \Treewec\Page\PageInfo */
    private $pageInfo;

    public function __construct($pageInfo, $view)
    {
        $this->pageInfo = $pageInfo;
        $this->view = new View();
        $this->loadTreewecView($view);
    }

    public function displayView()
    {
        $this->view->display();
    }

    public function error()
    {
        $this->view->addHeadingWithoutPrefix($this->pageInfo->header);
        $this->view->addView($this->treewecView, false);
    }

    public function source()
    {
        $cssClass = Sources\SourcesHelper::getSourceCSSClass($this->pageInfo->header);
        $this->view->addHeaderFromPageInfo($this->pageInfo);
        $this->view->addView($this->treewecView, true);
        $this->view->addContent('sources/page');
        $this->view->addDynamicData(array('cssClass' => $cssClass));
        $list = new Sources\SourceUsagePrinter();
        $list->setClass($cssClass);
        $list->setDataSource(new \Treewec\Iterators\Processors\Lists\ListDataSource());
        $list->setListing(new \Treewec\HTML\Listing());
        $list->setPrintLinks(true);
        $this->view->addPrinter($list, true);
    }

    public function todo()
    {
        $this->view->addContent('todo/page');
        $this->view->addDynamicData(array('todoFilePath' => TREEWEC_WEB_VIEW . '.todo.php'));
        $this->view->addPrinter(new TodoPrinter(), true);
    }

    public function file()
    {
        $this->view->addHeaderFromPageInfo($this->pageInfo);
        $this->view->addView($this->treewecView, true);
        $this->view->addContent('notes');
        $this->view->addContent('sources/list');
    }

    public function directory()
    {
        $this->view->addHeaderFromPageInfo($this->pageInfo);
        $this->view->addView($this->treewecView, true);
        $this->view->addPrinter($this->getObsahPrinter(false));
        $this->view->addContent('notes');
        $this->view->addContent('sources/list');
    }

    public function directoryAllInOne()
    {
        $this->view->addHeaderFromPageInfo($this->pageInfo);
        $this->view->addPrinter($this->getObsahPrinter(true));
        $this->view->addPrinter(new KnihaPrinter(), true);
        $this->view->addContent('notes');
        if (!isset($_GET['pages']) ||
            !in_array($this->pageInfo->currentPath, array('', TREETYPE_NAME_OF_SOURCES_DIR))
        ) {
            $this->view->addContent('sources/list');
        }
    }

    private function getObsahPrinter($internal)
    {
        $hierarchy = ListFacade::$NUMBER_HIERARCHY_LINKS;
        $obsahDataSource = new ObsahPrinter();
        $obsahDataSource->setInternalLinks($internal);
        $hierarchy->setDataSource($obsahDataSource);

        return $hierarchy;
    }

    public function indexesPage()
    {
        $indexes = new Indexes\Indexes($this->pageInfo->currentPath);
        $this->view->addHeaderFromPageInfo($this->pageInfo);
        $this->view->addContent('indexes');
        $this->view->addDynamicData(
            array(
                'illustrations' => $indexes->getIllustrationIndex(),
                'tables' => $indexes->getIndexOfTables(),
                'abbrevations' => $indexes->getAbbrevations(),
            )
        );
    }

    public function indexesDocument()
    {
        $this->view->addHeadingWithoutPrefix('Seznamy v celém dokumentu');
        $this->view->addContent('indexes');
        $this->view->addDynamicData($this->getIndexesForDocument());
    }

    private function getIndexesForDocument()
    {
        $printer = new Indexes\IndexesPrinter();
        $iterator = Functions::getIterator($printer);
        $iterator->run();
        return $printer->getIndexes();
    }

    public function analysisPage()
    {
        $this->view->addHeaderFromPageInfo($this->pageInfo);
        $this->view->addContent('analysis/page');
        $this->view->addDynamicData(
            array(
                'analyzer' => new Analysis\Analyzer($this->pageInfo->header->header, $this->pageInfo->currentPath),
                'words' => $this->getWordsForAnalysis()
            )
        );
    }

    public function analysisDocument()
    {
        $this->view->addHeadingWithoutPrefix('Analýza celého dokumentu');
        $this->view->addContent('analysis/search-form');
        $words = $this->getWordsForAnalysis();
        $printer = $words ? new Analysis\AnalyzerWordsPrinter($words) : new Analysis\AnalyzerPrinter();
        $this->view->addPrinter($printer, true);
    }

    private function getWordsForAnalysis()
    {
        if (isset($_POST['words'])) {
            $words = explode(';', $_POST['words']);
            $nonEmptyWords = array_filter($words, 'strlen');
            return array_unique($nonEmptyWords);
        }
        return array();
    }

    private function loadTreewecView($view)
    {
        $this->treewecView = $view;
        $isAdmin = is_int(strpos($_SERVER['REQUEST_URI'], '/admin/'));
        if (isset($_GET['typography']) && $_GET['typography'] == 'show') {
            if (!$isAdmin) {
                ob_start();
                $this->treewecView->display();
                $content = ob_get_contents();
                ob_clean();
                // real PHP is interpreted, but example code is converted to real code
                // in DomTypography::getPureXmlWithoutHtmlEntities
                $replace = array(
                    '<?php' => '&lt;?php',
                    '?>' => '?&gt;',
                );
            } else {
                if ($this->pageInfo->pageType == 'FILE') {
                    $content = file_get_contents($this->pageInfo->getFilePath());
                } else {
                    $content = file_get_contents($this->pageInfo->getFilePath() . 'index.php');
                }
                $content = \Treewec\Utils\Strings::codePHPInString($content);
                $replace = array(
                    // general in code or any other nonupdated element
                    '&lt;?php' => '&amp;lt;?php',
                    '?&gt;' => '?&amp;gt;',
                    // if in P (where typography is applied
                    '< ? Php' => '&amp;lt;?php',
                    '? >' => '?&amp;gt;',
                    '=>' => '=&amp;gt;',
                );
            }
            $this->treewecView = new \Treewec\Mvc\View();
            $typography = Typography\TypographyFactory::getDomTypography(
                $content,
                new Typography\TypographySelection()
            );
            $typography->updateDomDocument();
            $newContent = $typography->getContentOfBody();
            $newContent = str_replace(array_keys($replace), array_values($replace), $newContent);
            $this->treewecView->addContent($newContent);
        }
    }
}

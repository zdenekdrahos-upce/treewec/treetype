<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType;

final class Stats
{
    public static function getPercentage($amount, $total, $precision = 2)
    {
        if (is_numeric($amount) && is_numeric($total)) {
            if ($total != 0) {
                return round(100 * $amount / $total, $precision);
            }
        }
        return 0;
    }

    public static function getShare($amount, $total, $precision = 2)
    {
        if (is_numeric($amount) && is_numeric($total)) {
            if ($total != 0) {
                return round($amount / $total, $precision);
            }
        }
        return 0;
    }
}

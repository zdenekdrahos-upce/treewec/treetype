<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Typography;

use TreeType\Typography\Nbsp\CzechNbsp;

class TypographySelection
{
    private static $defaultTags = array(
        'p', 'strong', 'em', 'figcaption', 'mark', 'li', 'span', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'
    );

    public $tags;
    public $spaceAfterPunctuation;
    public $capitalizeFirstLetterInSentence;
    public $multipleSpacesReplace;
    public $nonBreakingSpaces;
    public $htmlEntitiyToChar;

    public function __construct()
    {
        $this->tags = self::$defaultTags;
        $this->spaceAfterPunctuation = true;
        $this->capitalizeFirstLetterInSentence = true;
        $this->multipleSpacesReplace = true;
        $this->nonBreakingSpaces = new CzechNbsp();
        $this->htmlEntitiyToChar = true;
    }
}

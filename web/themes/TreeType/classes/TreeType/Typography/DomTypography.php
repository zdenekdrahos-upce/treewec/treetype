<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Typography;

use TreeType\Typography\Action\IAction;

class DomTypography
{
    /** @var \DOMDocument */
    private $domDocument;
    private $actions;
    private $selectedTags;

    public function __construct(\DOMDocument $domDocument)
    {
        $this->domDocument = $domDocument;
        $this->actions = array();
        $this->selectedTags = array();
    }

    public function setTags($tags)
    {
        if (is_array($tags)) {
            $this->selectedTags = $tags;
        }
    }

    public function addAction($action)
    {
        if ($action instanceof IAction) {
            $this->actions[] = $action;
        }
    }

    public function updateDomDocument()
    {
        $xPath = new \DOMXPath($this->domDocument);
        foreach ($this->selectedTags as $tag) {
            foreach ($xPath->query("//{$tag}/text()") as $textnode) {
                foreach ($this->actions as $action) {
                    $textnode->nodeValue = $action->modify($textnode->nodeValue);
                }
            }
        }
    }

    public function getContentOfBody()
    {
        $xmlContent = $this->getPureXmlWithoutHtmlEntities();
        return $this->getContentInBodyTags($xmlContent);
    }

    private function getPureXmlWithoutHtmlEntities()
    {
        return html_entity_decode($this->domDocument->saveHtml(), ENT_QUOTES, "utf-8");
    }

    private function getContentInBodyTags($content)
    {
        $bodyStart = mb_strpos($content, '<body>', 0, 'UTF-8') + 6;
        $bodyEnd = mb_strpos($content, '</body>', 0, 'UTF-8');
        return mb_substr($content, $bodyStart, $bodyEnd - $bodyStart, 'UTF-8');
    }
}

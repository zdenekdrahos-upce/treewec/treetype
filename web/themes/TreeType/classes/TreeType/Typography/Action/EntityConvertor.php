<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Typography\Action;

use TreeType\Typography\HTMLEntities;

class EntityConvertor implements IAction
{
    const HTML_ENTITY_TO_CHAR = 'HTML_ENTITY_TO_CHAR';
    const CHAR_TO_HTML_ENTITY = 'CHAR_TO_HTML_ENTITY';

    private $convertType;

    public function __construct($convertType = self::HTML_ENTITY_TO_CHAR)
    {
        $this->convertType = $convertType;
    }

    public function modify($string)
    {
        if (is_string($string)) {
            return call_user_func(array($this, $this->getConvertMethod()), $string);
        }
        return $string;
    }

    private function getConvertMethod()
    {
        return $this->convertType == self::HTML_ENTITY_TO_CHAR ? 'convertEntitiesToChars' : 'convertCharsToEntites';
    }

    private function convertEntitiesToChars($string)
    {
        return str_replace(HTMLEntities::getHTMLEntities(), HTMLEntities::getUnicodeChars(), $string);
    }

    private function convertCharsToEntites($string)
    {
        return str_replace(HTMLEntities::getUnicodeChars(), HTMLEntities::getHTMLEntities(), $string);
    }
}

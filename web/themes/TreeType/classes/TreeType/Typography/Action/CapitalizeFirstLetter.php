<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Typography\Action;

// capitalize + add space after punctuation
class CapitalizeFirstLetter implements IAction
{
    private $punctuation;

    public function __construct()
    {
        $this->punctuation = array('.', '!', '?', '…');
    }

    public function modify($string)
    {
        if (is_string($string)) {
            return preg_replace(
                '/(?<!\d)([' . implode('', $this->punctuation) . '])\s*(\w)/e',
                "strtoupper('\\1 \\2')",
                $string
            );
        }
        return $string;
    }
}

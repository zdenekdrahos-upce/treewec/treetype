<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Typography\Action;

use TreeType\Typography\Nbsp\Nbsp;
use TreeType\Typography\HTMLEntities;

class NonBreakingSpaceAdd implements IAction
{
    private $nonBreakableSpace;
    private $lowerAndUpperFormats;
    private $specificFormats;
    private $regexFormats;

    public function __construct()
    {
        $this->nonBreakableSpace = HTMLEntities::getUnicodeChar('&nbsp;');
        $this->lowerAndUpperFormats = array();
        $this->specificFormats = array();
        $this->regexFormats = array();
    }

    public function modify($string)
    {
        if (is_string($string)) {
            $string = $this->addLowerUpperFormats($string);
            $string = $this->addSpecificFormats($string);
            $string = $this->addRegexFormats($string);
        }
        return $string;
    }

    public function addFormats($formats)
    {
        if ($formats instanceof Nbsp) {
            $this->appendFormats('lowerAndUpperFormats', $formats->getLowerAndUpperFormats());
            $this->appendFormats('specificFormats', $formats->getSpecificFormats());
            $this->appendFormats('regexFormats', $formats->getRegexFormats());
        }
    }

    private function appendFormats($attribute, $newFormats)
    {
        if (is_array($newFormats) && !empty($newFormats)) {
            $this->$attribute = array_merge($this->$attribute, $newFormats);
        }
    }

    private function addLowerUpperFormats($string)
    {
        return $this->addCharFormats($string, $this->lowerAndUpperFormats, true);
    }

    private function addSpecificFormats($string)
    {
        return $this->addCharFormats($string, $this->specificFormats, false);
    }

    private function addCharFormats($string, $formats, $toupper = false)
    {
        $beforeChar = substr($string, 0, 1) == ' ' ? '' : ' ';
        $afterChar = substr($string, -1) == ' ' ? '' : ' ';
        $string = $beforeChar . $string . $afterChar;
        foreach ($formats as $wordWithSpace) {
            $beforeSpace = substr($wordWithSpace, 0, 1) == ' ' ? '' : ' ';
            $afterSpace = substr($wordWithSpace, -1) == ' ' ? '' : ' ';
            $string = str_replace(
                $beforeSpace . $wordWithSpace . $afterSpace,
                $beforeSpace . str_replace(' ', $this->nonBreakableSpace, $wordWithSpace) . $afterSpace,
                $string
            );
            if ($toupper) {
                $string = str_replace(
                    $beforeSpace . mb_strtoupper($wordWithSpace, 'UTF-8') . $afterSpace,
                    $beforeSpace . str_replace(
                        ' ',
                        $this->nonBreakableSpace,
                        mb_strtoupper($wordWithSpace, 'UTF-8')
                    ) . $afterSpace,
                    $string
                );
            }
        }
        if ($beforeChar === ' ') {
            $string = mb_substr($string, 1, mb_strlen($string, 'UTF-8'), 'UTF-8');
        }
        if ($afterChar === ' ') {
            $string = mb_substr($string, 0, -1, 'UTF-8');
        }
        return $string;
    }

    private function addRegexFormats($string)
    {
        return preg_replace(array_keys($this->regexFormats), array_values($this->regexFormats), $string);
    }
}

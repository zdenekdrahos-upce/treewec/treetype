<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Typography\Action;

class SpaceAfterPunctuationAdd implements IAction
{
    private $punctuation;

    public function __construct()
    {
        $this->punctuation = array(',', '.', ':', ';', '…', '!', '?');
    }

    public function addPunctuation($punctuation)
    {
        if (is_array($punctuation)) {
            $this->punctuation = array_merge($this->punctuation, $punctuation);
            $this->punctuation = array_unique($this->punctuation);
        }
    }

    public function modify($string)
    {
        if (is_string($string)) {
            return rtrim(
                preg_replace(
                    '/(?<!\d)([' . implode('', $this->punctuation) . '])\s*(\w)/e',
                    "'\\1 \\2'",
                    $string
                ),
                " "
            );
        }
        return $string;
    }
}

<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Typography\Action;

interface IAction
{
    /**
     * @param  string $string
     * @return string
     */
    public function modify($string);
}

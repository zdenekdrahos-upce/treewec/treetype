<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Typography\Action;

class MultipleSpacesReplace implements IAction
{
    public function modify($string)
    {
        if (is_string($string)) {
            $stringWithoutMultipleSpaces = preg_replace("/[[:blank:]]+/u", " ", $string);
            return str_replace('&nbsp; ', '&nbsp;', $stringWithoutMultipleSpaces);
        }
        return $string;
    }
}

<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Typography;

use TreeType\Typography\Nbsp\Nbsp;
use TreeType\Typography\Action\EntityConvertor;
use TreeType\Typography\Action\CapitalizeFirstLetter;
use TreeType\Typography\Action\MultipleSpacesReplace;
use TreeType\Typography\Action\NonBreakingSpaceAdd;
use TreeType\Typography\Action\SpaceAfterPunctuationAdd;

class TypographyFactory
{
    /** @return \TreeType\Typography\DomTypography */
    public static function getDomTypographyFromPath($path, TypographySelection $selection)
    {
        $content = \TreeType\Functions::getFileContent($path);
        return self::getDomTypography($content, $selection);
    }

    /** @return \TreeType\Typography\DomTypography */
    public static function getDomTypography($content, TypographySelection $selection)
    {
        $typography = new DomTypography(\HTML5_Parser::parse($content));
        $typography->setTags($selection->tags);
        if ($selection->spaceAfterPunctuation) {
            $typography->addAction(new SpaceAfterPunctuationAdd());
        }
        if ($selection->multipleSpacesReplace) {
            $typography->addAction(new MultipleSpacesReplace());
        }
        if ($selection->capitalizeFirstLetterInSentence) {
            $typography->addAction(new CapitalizeFirstLetter());
        }
        if ($selection->nonBreakingSpaces instanceof Nbsp) {
            $nonBreakingSpaces = new NonBreakingSpaceAdd();
            $nonBreakingSpaces->addFormats($selection->nonBreakingSpaces);
            $typography->addAction($nonBreakingSpaces);
        }
        if ($selection->htmlEntitiyToChar) {
            $typography->addAction(new EntityConvertor(EntityConvertor::HTML_ENTITY_TO_CHAR));
        }
        return $typography;
    }
}

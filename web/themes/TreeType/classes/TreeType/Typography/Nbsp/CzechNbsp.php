<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Typography\Nbsp;

use TreeType\Typography\HTMLEntities;

class CzechNbsp extends Nbsp
{
    public static function getLowerAndUpperFormats()
    {
        return array(
            'v ', 's ', 'z ', 'k ', 'a ', 'i ', 'o ', 'u ',
        );
    }

    public static function getSpecificFormats()
    {
        return array(
            'a. s.', 's. r. o.', 'mn. č.', 'př. n. l.',
            'tj. ', 'tzv.', 'tzn. ',
            ' Kč', ' kg', ' h', ' %', ' : ',
            'p. ', 'mjr. ', 'Bc. ', 'Ing. ', 'DiS. ',
        );
    }

    public static function getRegexFormats()
    {
        return array(
            '/([0-9]). ([0-9])/' => '$1.' . HTMLEntities::getUnicodeChar('&nbsp;') . '$2',
            '/([0-9])-([0-9])/' => '$1' . HTMLEntities::getUnicodeChar('&#8209;') . '$2',
        );
    }
}

<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Typography\Nbsp;

class Nbsp
{
    public static function getLowerAndUpperFormats()
    {
        return array();
    }

    public static function getSpecificFormats()
    {
        return array();
    }

    public static function getRegexFormats()
    {
        return array();
    }
}

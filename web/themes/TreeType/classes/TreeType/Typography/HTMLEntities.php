<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Typography;

final class HTMLEntities
{
    private static $entities = array(
        # BASIC
        '&nbsp;' => ' ',// non-breaking space
        '&hellip;' => '…',// three dot leader
        '&lt;' => '<',
        '&gt;' => '>',
        # DASHES AND MINUS
        '&minus;' => '−',// mathematical minus sign
        '-' => '-',// hyphen - no html entity
        '&#8209;' => '‑',// non-breaking hyphen
        '&ndash;' => '–',// en dash
        '&mdash;' => '—',// em dash
        # ARROWS
        '&larr;' => '←',
        '&uarr;' => '↑',
        '&rarr;' => '→',
        '&darr;' => '↓',
    );

    public static function getHTMLEntities()
    {
        return array_keys(self::$entities);
    }

    public static function getUnicodeChars()
    {
        return array_values(self::$entities);
    }

    public static function getHTMLEntity($dash)
    {
        if (is_string($dash) && in_array($dash, self::$entities)) {
            return array_search($dash, self::$entities);
        }
        return $dash;
    }

    public static function getUnicodeChar($dash)
    {
        if (is_string($dash) && array_key_exists($dash, self::$entities)) {
            return self::$entities[$dash];
        }
        return $dash;
    }
}

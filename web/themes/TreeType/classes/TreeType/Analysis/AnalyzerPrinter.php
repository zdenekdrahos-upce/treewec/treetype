<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Analysis;

use Treewec\Iterators\Process\ISurroundedProcessing;
use Treewec\Output\OutputFileHelper;
use \SplFileInfo;

class AnalyzerPrinter implements ISurroundedProcessing
{
    private $pageCount = 0;
    private $wordCount = 0;
    private $allCharCount = 0;
    private $nonSpaceCharCount = 0;
    /** @var \Treewec\Output\OutputFileHelper */
    private $outputter;

    public function __construct()
    {
        $this->outputter = new OutputFileHelper(TREEWEC_WEB_VIEW);
    }

    public function beforeIteration()
    {
        include($this->getViewTemplate());
    }

    public function process(SplFileInfo $splFileInfo, $depth)
    {
        $header = $this->outputter->getFilename($splFileInfo, ' ');
        $basePathForUrlLink = $this->outputter->getBaseLinkForURL($splFileInfo);
        $analyzer = new Analyzer($header, $basePathForUrlLink);
        $this->updateCounts($analyzer);
        include($this->getViewTemplate());
    }

    private function updateCounts($analyzer)
    {
        $this->pageCount++;
        $this->wordCount += $analyzer->getWordCount();
        $this->allCharCount += $analyzer->getAllCharCount();
        $this->nonSpaceCharCount += $analyzer->getCharCount();
    }

    private function getViewTemplate()
    {
        return TREETYPE_VIEW_PATH . 'analysis/document.php';
    }

    public function afterIteration()
    {
        include($this->getViewTemplate());
    }
}

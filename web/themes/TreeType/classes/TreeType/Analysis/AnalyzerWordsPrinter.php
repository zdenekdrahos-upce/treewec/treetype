<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Analysis;

use Treewec\Iterators\Process\ISurroundedProcessing;
use Treewec\Output\OutputFileHelper;
use \SplFileInfo;

class AnalyzerWordsPrinter implements ISurroundedProcessing
{
    private $pageCount = 0;
    private $searchedWords;
    private $wordsCount = array();

    public function __construct($searchedWords)
    {
        $this->searchedWords = $searchedWords;
        $this->outputter = new OutputFileHelper(TREEWEC_WEB_VIEW);
        $this->wordsCount = array('caseSensitive' => 0,'caseInsensitive' => 0);
    }

    public function beforeIteration()
    {
        include($this->getViewTemplate());
    }

    public function process(SplFileInfo $splFileInfo, $depth)
    {
        $header = $this->outputter->getFilename($splFileInfo, ' ');
        $basePathForUrlLink = $this->outputter->getBaseLinkForURL($splFileInfo);
        $analyzer = new Analyzer($header, $basePathForUrlLink);
        $caseSensitive = 0;
        $caseInsensitive = 0;
        $this->updateCounts($analyzer, $caseSensitive, $caseInsensitive);
        include($this->getViewTemplate());
    }

    private function updateCounts($analyzer, &$caseSensitiveCount, &$caseInsensitiveCount)
    {
        $this->pageCount++;
        foreach ($this->searchedWords as $word) {
            $usage = $analyzer->getWordUsage($word);
            $caseSensitiveCount += $usage['caseSensitive'];
            $caseInsensitiveCount += $usage['caseInsensitive'];
        }
        $this->wordsCount['caseSensitive'] += $caseSensitiveCount;
        $this->wordsCount['caseInsensitive'] += $caseInsensitiveCount;
    }

    private function getViewTemplate()
    {
        return TREETYPE_VIEW_PATH . 'analysis/words-usage.php';
    }

    public function afterIteration()
    {
        include($this->getViewTemplate());
    }
}

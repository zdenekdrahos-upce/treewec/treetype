<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Analysis;

use TreeType\Typography\Action\EntityConvertor;

final class Analyzer
{
    private $content;
    private $wordCount;
    private $charCounts;

    public function __construct($header, $path)
    {
        $this->loadContent($header, $path);
        $this->loadWordCount();
        $this->loadCharCount();
    }

    public function getWordCount()
    {
        return $this->wordCount;
    }

    public function getWordUsage($word)
    {
        return array(
            'caseSensitive' => mb_substr_count($this->content, $word, 'UTF-8'),
            'caseInsensitive' => mb_substr_count($this->lowerContent, mb_strtolower($word, 'UTF-8'), 'UTF-8'),
        );
    }

    public function getAllCharCount()
    {
        return mb_strlen($this->content, 'UTF-8');
    }

    public function getCharCount()
    {
        return array_sum($this->charCounts);
    }

    public function getCharCountArray()
    {
        return $this->charCounts;
    }

    private function loadContent($header, $path)
    {
        $fileContent = $header . \TreeType\Functions::getFileContent($path);
        $contentWithoutTags = strip_tags($fileContent);
        $this->content = $this->getContentWithoutHTMLEntities($contentWithoutTags);
        $this->lowerContent = mb_strtolower($this->content, 'UTF-8');
    }

    private function getContentWithoutHTMLEntities($content)
    {
        $converter = new EntityConvertor(EntityConvertor::HTML_ENTITY_TO_CHAR);
        return $converter->modify($content);
    }

    private function loadWordCount()
    {
        $alphanumericContent = preg_replace('/[^\p{L}\p{N}\s]/u', '', $this->content);
        $words = preg_split('~[^\p{L}\p{N}\']+~u', $alphanumericContent);
        $wordsWithoutEmptyValues = array_filter($words, 'strlen');
        $this->wordCount = count($wordsWithoutEmptyValues);
    }

    private function loadCharCount()
    {
        $contentWithoutSpaces = preg_replace('/\s+/', '', $this->content);
        $this->charCounts = \TreeType\Functions::mbCountChars($contentWithoutSpaces);
        uksort($this->charCounts, 'strcasecmp');
    }
}

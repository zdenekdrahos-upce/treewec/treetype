<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Sources;

use Treewec\Iterators\Processors\Lists\ListPrinter;

class SourceUsagePrinter extends ListPrinter
{
    private $sourceClass;

    public function setClass($class)
    {
        $this->sourceClass = $class;
    }

    public function process(\SplFileInfo $splFileInfo, $depth)
    {
        if ($this->isClassUsedOnPage($splFileInfo)) {
            parent::process($splFileInfo, $depth);
        }
    }

    private function isClassUsedOnPage(\SplFileInfo $splFileInfo)
    {
        $fileContent = file_get_contents($this->getIncludePath($splFileInfo));
        return is_int(strpos($fileContent, $this->sourceClass));
    }

    private function getIncludePath($splFileInfo)
    {
        if ($splFileInfo->isDir()) {
            return $splFileInfo->getPathname() . '/index.php';
        } else {
            return $splFileInfo->getPathname();
        }
    }
}

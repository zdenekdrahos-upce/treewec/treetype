<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Sources;

use Treewec\Iterators\Processors\Lists\ListDataSource;
use Treewec\HTML\AttributeFactory;

class SourcesPrinter extends ListDataSource
{

    private $count = 1;

    public function getItemAttributes()
    {
        return array(
            AttributeFactory::getID((string) $this->count++)
        );
    }

    public function getItemContent()
    {
        ob_start();
        include($this->file->getPathname());
        $content = ob_get_contents();
        ob_clean();
        return $content;
    }
}

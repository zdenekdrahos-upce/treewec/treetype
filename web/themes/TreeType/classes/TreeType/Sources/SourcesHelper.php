<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Sources;

use Treewec\FileSystem\Helpers\DirectoryOrder;

final class SourcesHelper
{
    public static function printJavascriptArray()
    {
        $links = array();
        foreach (SourcesHelper::getAvailableSources() as $number => $sourceClass) {
            $link = \TreeType\Elements\Links::source($number);
            $links[] = "'{$sourceClass}': '{$link}'";
        }
        echo '{' . implode(', ', $links) . '}';
    }

    public static function printIfHideNotUsedLinks($pageInfo)
    {
        $hide = !is_int(strpos($pageInfo->currentPath, TREETYPE_NAME_OF_SOURCES_DIR));
        echo $hide ? 'true' : 'false';
    }

    public static function getStylesSource()
    {
        $sources = array();
        foreach (SourcesHelper::getAvailableSources() as $sourceClass) {
            $sources[$sourceClass] = \Treewec\Utils\Strings::uppercaseFirstLetterAndReplaceDashes($sourceClass);
        }
        return $sources;
    }

    public static function getStylesCKEditorElements()
    {
        return array('p');
    }

    private static function getAvailableSources()
    {
        $sources = array();
        $dirOrder = new DirectoryOrder();
        $path = \Treewec\FileSystem\Path::toFileSystem(TREETYPE_NAME_OF_SOURCES_DIR);
        if ($dirOrder->changeDirectory(TREEWEC_WEB_VIEW . $path)) {
            foreach ($dirOrder->getOrderArray() as $filename => $number) {
                $filenameWithoutExtension = substr($filename, 0, -4);
                $sources[$number + 1] = self::getSourceCSSClass($filenameWithoutExtension);
            }
        }
        return $sources;
    }

    public static function getSourceCSSClass($filename)
    {
        return mb_strtolower(str_replace(' ', '-', $filename), 'UTF-8');
    }
}

<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType;

use TreeType\Elements\Links;

function note($name, $text)
{
    Links::note($name, $text);
}

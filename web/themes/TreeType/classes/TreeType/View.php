<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType;

use TreeType\Elements\Headings;

class View
{
    /** @var array */
    private $viewParts;
    /** @var array */
    private $dynamicData;
    /** @var boolean */
    private $displayCombinedView;

    public function __construct()
    {
        $this->displayCombinedView = false;
        $this->viewParts = array();
        $this->dynamicData = array();
    }

    public function display()
    {
        if ($this->displayCombinedView) {
            $this->displayCombinedView();
        } else {
            $this->displayClassicView();
        }
    }

    public function addHeaderFromPageInfo($pageInfo)
    {
        $this->viewParts[] = Headings::getHeading($pageInfo);
    }

    public function addHeadingWithoutPrefix($header)
    {
        $this->viewParts[] = "<h1 class=\"noPrefix\">{$header}</h1>";
    }

    public function addContent($viewName)
    {
        $this->viewParts[] = new \Treewec\FileSystem\File(TREETYPE_VIEW_PATH . $viewName . '.php');
    }

    public function addView($view, $contentEditable)
    {
        $this->displayCombinedView = true;
        $this->viewParts[] = array('view' => $view, 'contentEditable' => $contentEditable);
    }

    public function addPrinter($printer, $startRoot = false)
    {
        $iterator = Functions::getIterator($printer, $startRoot);
        $this->addIterator($iterator);
    }

    public function addIterator(\Treewec\Iterators\IteratorFacade $iterator)
    {
        $this->displayCombinedView = true;
        $this->viewParts[] = $iterator;
    }

    public function addDynamicData($data)
    {
        $this->dynamicData = $data;
    }

    private function displayClassicView()
    {
        $view = $this->getInitializedTreewecView();
        foreach ($this->viewParts as $part) {
            $view->addContent($part);
        }
        $view->display();
    }

    private function displayCombinedView()
    {
        $displayed = false;
        $view = $this->getInitializedTreewecView();
        foreach ($this->viewParts as $part) {
            if (is_array($part) || $part instanceof \Treewec\Iterators\IteratorFacade) {
                $view->display();
                $displayed = true;
                if (is_array($part)) {
                    $this->displayTreewecView($part);
                } else {
                    $this->displayIterator($part);
                }
                $view = $this->getInitializedTreewecView();
            } else {
                $displayed = false;
                $view->addContent($part);
            }
        }
        if (!$displayed) {
            $view->display();
        }
    }

    private function displayTreewecView($viewArray)
    {
        if ($viewArray['contentEditable']) {
            \Treewec\Mvc\ViewDisplayer::display($viewArray['view']);
        } else {
            $viewArray['view']->display();
        }
    }

    private function displayIterator($iterator)
    {
        $iterator->run();
    }

    private function getInitializedTreewecView()
    {
        $view = new \Treewec\Mvc\View();
        $view->addData($this->dynamicData);
        return $view;
    }
}

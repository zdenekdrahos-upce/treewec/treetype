<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Iterators;

use Treewec\Iterators\Processors\Lists\ListDataSource;
use Treewec\HTML\AttributeFactory;
use TreeType\Elements\Headings;

class ObsahPrinter extends ListDataSource
{
    public function setInternalLinks($internal = false)
    {
        $this->internal = $internal;
    }

    public function getListAttributes()
    {
        return array(
            AttributeFactory::getClass('content')
        );
    }

    public function getItemContent()
    {
        if ($this->internal) {
            $header = $this->outputter->getFilename($this->file);
            $id = Headings::convertHeaderToID($header);
            $currentPath = \Treewec\TreewecPage::getPageInfo()->currentPath;
            echo \Treewec\HTML\Anchor::create($currentPath . '&pages=all#' . $id, $header);
        } else {
            return parent::getItemContent();
        }
    }
}

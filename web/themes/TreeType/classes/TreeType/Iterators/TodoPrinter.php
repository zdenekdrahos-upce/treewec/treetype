<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Iterators;

use Treewec\Iterators\Process\IElementProcessing;
use Treewec\Output\OutputFileHelper;
use Treewec\Url\UrlBuilder;
use \SplFileInfo;
use TreeType\DOMHelper;

class TodoPrinter implements IElementProcessing
{
    /** @var \Treewec\Output\OutputFileHelper */
    private $outputter;
    /** @var \Treewec\Url\UrlBuilder */
    private $urlBuilder;

    public function __construct()
    {
        $this->outputter = new OutputFileHelper(TREEWEC_WEB_VIEW);
        $this->urlBuilder = UrlBuilder::getEmpty();
    }

    public function process(SplFileInfo $splFileInfo, $depth)
    {
        $marks = $this->getMarks($splFileInfo);
        $header = $this->outputter->getFilename($splFileInfo, ' ');
        $basePathForUrlLink = $this->outputter->getBaseLinkForURL($splFileInfo);
        include(TREETYPE_VIEW_PATH . 'todo/marks.php');
    }

    private function getMarks($splFileInfo)
    {
        $basePathForUrlLink = $this->outputter->getBaseLinkForURL($splFileInfo);
        $domDocument = DOMHelper::getDomDocument($basePathForUrlLink);
        $marks = array();
        $elements = $domDocument->getElementsByTagName('mark');
        foreach ($elements as $element) {
            $marks[] = $element->nodeValue;
        }
        return $marks;
    }
}

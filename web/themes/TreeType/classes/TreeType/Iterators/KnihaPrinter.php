<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Iterators;

use Treewec\Iterators\Process\IElementProcessing;
use Treewec\Output\OutputFileHelper;
use Treewec\Url\UrlBuilder;
use \SplFileInfo;
use TreeType\Elements\Headings;

class KnihaPrinter implements IElementProcessing
{
    /** @var \Treewec\Output\OutputFileHelper */
    private $outputter;
    /** @var \Treewec\Url\UrlBuilder */
    private $urlBuilder;

    public function __construct()
    {
        $this->outputter = new OutputFileHelper(TREEWEC_WEB_VIEW);
        $this->urlBuilder = UrlBuilder::getEmpty();
    }

    public function process(SplFileInfo $splFileInfo, $depth)
    {
        $sourcesFolder = \Treewec\FileSystem\Path::toFileSystem(substr(TREETYPE_NAME_OF_SOURCES_DIR, 0, -1));
        $isSource = is_int(strpos($splFileInfo->getPathname(), $sourcesFolder));
        if (!$isSource || ($splFileInfo->isDir() && $isSource)) {
            $this->displayView($splFileInfo, $isSource);
        }
    }
    private function displayView(SplFileInfo $splFileInfo, $isSource)
    {
        $header = $this->outputter->getFilename($splFileInfo, ' ');
        $basePathForUrlLink = $this->outputter->getBaseLinkForURL($splFileInfo);
        $id = Headings::convertHeaderToID($header);
        $tag = $this->getTag($basePathForUrlLink);
        $includedPath = $this->getIncludePath($splFileInfo, $isSource);
        include(TREETYPE_VIEW_PATH . 'page-in-list.php');
    }

    private function getTag($basePath)
    {
        if ($basePath !== '') {
            $values = explode('/', $basePath);
            $values = array_filter($values, 'strlen');
            $tag = 'h' . count($values);
            return array('start' => $tag, 'end' => $tag);
        } else {
            return array('start' => 'h1 class="noPrefix"', 'end' => 'h1');
        }
    }

    private function getIncludePath($splFileInfo, $isSource)
    {
        if ($splFileInfo->isFile()) {
            return $splFileInfo->getPathname();
        } elseif ($splFileInfo->isDir() && !$isSource) {
            return $splFileInfo->getPathname() . '/index.php';
        } elseif ($splFileInfo->isDir()) {
            return TREEWEC_WEB_THEME . 'view/sources/list.php';
        }
    }
}

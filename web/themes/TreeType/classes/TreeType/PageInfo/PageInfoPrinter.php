<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\PageInfo;

use Treewec\Page\PageInfo;
use Treewec\Page\OnePage;

final class PageInfoPrinter
{
    public static function printBreadcrumbNavigation($pageInfo, $separator = ' ', $printLinks = true)
    {
        if ($pageInfo instanceof PageInfo) {
            self::printPage($pageInfo->homePage, '', $printLinks);
            foreach ($pageInfo->pathInfo as $page) {
                echo $separator;
                self::printPage($page, '', $printLinks);
            }
        }
    }

    public static function printPage($page, $text = '', $printLinks = true)
    {
        if ($page instanceof OnePage) {
            if ($printLinks) {
                echo $page->getLink($text);
            } else {
                echo $page->header;
            }
        } else {
            echo '-';
        }
    }
}

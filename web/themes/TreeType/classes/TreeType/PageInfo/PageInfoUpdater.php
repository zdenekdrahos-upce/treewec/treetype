<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\PageInfo;

use Treewec\Page\PageInfo;
use Treewec\Page\OnePage;

final class PageInfoUpdater
{
    public static function updateHeaders(&$pageInfo)
    {
        if ($pageInfo instanceof PageInfo) {
            self::updatePage($pageInfo->header);
            self::updatePage($pageInfo->homePage);
            self::updatePage($pageInfo->previousPage);
            self::updatePage($pageInfo->nextPage);
            self::updatePage($pageInfo->parentFolder);
            foreach ($pageInfo->pathInfo as $page) {
                self::updatePage($page);
            }
        }
    }

    private static function updatePage(&$page)
    {
        if ($page instanceof OnePage &&
            !in_array($page->header, array(TREEWEC_HEADER_MAINPAGE, TREEWEC_HEADER_ERROR), true)
        ) {
            $page->header = self::getModifiedHeader($page->header);
        }
    }

    private static function getModifiedHeader($header)
    {
        return \Treewec\Utils\Strings::uppercaseFirstLetterAndReplaceDashes($header, ' ');
    }
}

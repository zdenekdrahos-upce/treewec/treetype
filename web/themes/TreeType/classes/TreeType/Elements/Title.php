<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Elements;

final class Title
{
    public static function getTitle($pageInfo)
    {
        if ($pageInfo->currentPath == '') {
            return TREEWEC_HEADER_MAINPAGE;
        } else {
            return TREEWEC_HEADER_MAINPAGE . ' | ' .
                \Treewec\Utils\Arrays::convertArrayToString($pageInfo->pathInfo, ' &raquo; ');
        }
    }
}

<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Elements;

final class Links
{
    private static $allNotes = array();

    public static function note($name, $text)
    {
        self::$allNotes[$name] = $text;
        $class = \Treewec\HTML\AttributeFactory::getClass('note');
        $title = \Treewec\HTML\AttributeFactory::getTitle($text);
        echo self::internalLink($name, '', array($class, $title));
    }

    public static function source($number)
    {
        $class = \Treewec\HTML\AttributeFactory::getClass('src');
        return self::internalLink($number, "[{$number}]", array($class));
    }

    public static function internalLink($name, $text, $attributes)
    {
        $urlBuilder = \Treewec\TreewecPage::getUrlBuilder();
        return \Treewec\HTML\Anchor::create($urlBuilder->build() . "#{$name}", $text, $attributes);
    }

    public static function existsNotes()
    {
        return !empty(self::$allNotes);
    }

    public static function getNotes()
    {
        return self::$allNotes;
    }

    public static function resetNotes()
    {
        self::$allNotes = array();
    }
}

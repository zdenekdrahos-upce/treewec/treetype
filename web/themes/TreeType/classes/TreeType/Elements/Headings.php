<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Elements;

class Headings
{
    public static function getHeading($pageInfo)
    {
        $class = \Treewec\HTML\AttributeFactory::getClass(
            'noPrefix',
            $pageInfo->currentPath == '' || isset($_GET['pages'])
        );
        return self::getTag($pageInfo->header, 1, $class);
    }

    private static function getTag($text, $level, $class = null)
    {
        $class = $class instanceof \Treewec\HTML\Attribute ? " {$class}" : '';
        return "<h{$level}{$class}>{$text}</h{$level}>";
    }

    public static function convertHeaderToID($header)
    {
        $headerWithReplacedSpaces = str_replace(' ', '-', $header);
        $lowerHeader = mb_strtolower($headerWithReplacedSpaces, 'UTF-8');
        return $lowerHeader;
    }
}

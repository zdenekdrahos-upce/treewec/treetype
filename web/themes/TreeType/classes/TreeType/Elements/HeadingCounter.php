<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Elements;

use Treewec\FileSystem\Helpers\DirectoryOrder;

class HeadingCounter
{
    public static function getPrefixForPage($pageInfo)
    {
        $value = '';
        for ($i = 1; $i <= 4; $i++) {
            if (array_key_exists($i - 1, $pageInfo->pathInfo)) {
                $number = HeadingCounter::getValue($pageInfo, $i);
                $value .= ++$number . '.';
            }
        }
        return $value;
    }

    public static function getValue($pageInfo, $level)
    {
        if (array_key_exists($level - 1, $pageInfo->pathInfo)) {
            $filename = self::getFileName($pageInfo, $level);
            $dirOrder = new DirectoryOrder();
            $path = $level - 1 == 0 ? '': $pageInfo->pathInfo[$level - 2]->path;
            $path = \Treewec\FileSystem\Path::toFileSystem($path);
            if ($dirOrder->changeDirectory(TREEWEC_WEB_VIEW . $path)) {
                $orderArray = $dirOrder->getOrderArray();
                if (array_key_exists($filename, $orderArray)) {
                    return $orderArray[$filename];
                }
            }
            return 0;
        }
        return false;
    }

    private static function getFileName($pageInfo, $level)
    {
        $page = $pageInfo->pathInfo[$level - 1];
        $name = $page->header;
        $ext = substr($page->path, -1) == '/' ? '' : TREEWEC_FILES_EXTENSION;
        return str_replace(' ', '-', mb_strtolower($name, 'UTF-8')) . $ext;
    }
}

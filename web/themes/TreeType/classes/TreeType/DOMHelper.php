<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType;

class DOMHelper
{
    public static function getDomDocument($path)
    {
        $content = Functions::getFileContent($path);
        return \HTML5_Parser::parse($content);
    }

    public static function getFigcaption($domElement)
    {
        return self::getValue($domElement, 'figcaption');
    }

    public static function getTable($domElement)
    {
        $table = self::getValue($domElement, 'table');
        if ($table) {
            return self::transformNodeToHTML($table);
        }
        return '';
    }

    public static function getImage($domElement)
    {
        $tags = array('img', 'embed');
        foreach ($tags as $tag) {
            $images = self::getValue($domElement, $tag);
            $html = self::transformNodeToHTML($images);
            if ($html) {
                return $html;
            }
        }
        return '';
    }

    private static function getValue($domElement, $tagName)
    {
        if ($domElement instanceof \DOMElement) {
            $elements = $domElement->getElementsByTagName($tagName);
            if ($elements->length > 0) {
                return $elements->item(0);
            }
        }
        return '';
    }

    private static function transformNodeToHTML($domElement)
    {
        if ($domElement instanceof \DOMElement) {
            $newdoc = new \DOMDocument();
            $newdoc->appendChild($newdoc->importNode($domElement, true));
            return $newdoc->saveHTML();
        }
        return '';
    }
}

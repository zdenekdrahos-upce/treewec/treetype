<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Indexes;

final class Indexes
{
    private $path;
    /** @var DOMDocument */
    private $domDocument;
    private $illustrations = array();
    private $tables = array();
    private $abbrevations = array();

    public function __construct($path)
    {
        $this->domDocument = \TreeType\DOMHelper::getDomDocument($path);
        $this->path = $path;
        $this->loadFigures();
        $this->loadAbbrevations();
    }

    public function getIndexOfTables()
    {
        return $this->tables;
    }

    public function getIllustrationIndex()
    {
        return $this->illustrations;
    }

    public function getAbbrevations()
    {
        return $this->abbrevations;
    }

    private function loadFigures()
    {
        $elements = $this->domDocument->getElementsByTagName('figure');
        foreach ($elements as $element) {
            if ($element->getAttribute('class') == 'img') {
                $this->illustrations[] = IndexItem::getIllustration($element, $this->path);
            } elseif ($element->getAttribute('class') == 'table') {
                $this->tables[] = IndexItem::getTable($element, $this->path);
            }
        }
    }

    private function loadAbbrevations()
    {
        $elements = $this->domDocument->getElementsByTagName('abbr');
        foreach ($elements as $element) {
            $this->abbrevations[] = IndexItem::getAbbrevation($element, $this->path);
        }
    }
}

<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Indexes;

use Treewec\Iterators\Process\IElementProcessing;
use Treewec\Output\OutputFileHelper;
use \SplFileInfo;

class IndexesPrinter implements IElementProcessing
{
    /** @var \Treewec\Output\OutputFileHelper */
    private $outputter;
    private $illustrations = array();
    private $tables = array();
    private $abbrevations = array();

    public function __construct()
    {
        $this->outputter = new OutputFileHelper(TREEWEC_WEB_VIEW);
    }

    public function process(SplFileInfo $splFileInfo, $depth)
    {
        $basePathForUrlLink = $this->outputter->getBaseLinkForURL($splFileInfo);
        $indexes = new Indexes($basePathForUrlLink);
        $this->illustrations = array_merge($this->illustrations, $indexes->getIllustrationIndex());
        $this->tables = array_merge($this->tables, $indexes->getIndexOfTables());
        $this->abbrevations = array_merge($this->abbrevations, $indexes->getAbbrevations());
    }

    public function getIndexes()
    {
        return array(
            'illustrations' => $this->illustrations,
            'tables' => $this->tables,
            'abbrevations' => $this->abbrevations,
        );
    }
}

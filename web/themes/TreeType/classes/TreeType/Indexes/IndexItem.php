<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType\Indexes;

class IndexItem
{
    private $content = '';
    private $caption = '';
    private $path = '';

    private function __construct($path)
    {
        $this->path = $path;
    }

    public static function getIllustration($domElement, $path = '')
    {
        $instance = new self($path);
        $instance->content = \TreeType\DOMHelper::getImage($domElement);
        $instance->caption = \TreeType\DOMHelper::getFigcaption($domElement);
        return $instance;
    }

    public static function getTable($domElement, $path = '')
    {
        $instance = new self($path);
        $instance->content = \TreeType\DOMHelper::getTable($domElement);
        $instance->caption = \TreeType\DOMHelper::getFigcaption($domElement);
        return $instance;
    }

    public static function getAbbrevation($domElement, $path = '')
    {
        $instance = new self($path);
        $instance->content = $domElement->getAttribute('title');
        $instance->caption = $domElement->nodeValue;
        return $instance;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getContent()
    {
        if ($this->content) {
            return $this->content;
        }
        return '???';
    }

    public function __toString()
    {
        if ($this->caption) {
            return $this->caption instanceof \DOMElement ? $this->caption->nodeValue : $this->caption;
        }
        return '???';
    }
}

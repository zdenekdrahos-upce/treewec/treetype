<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType;

use Treewec\Iterators\Processors\Lists\ListFacade;

final class Functions
{
    /** http://www.php.net/manual/en/function.count-chars.php#107336 */
    public static function mbCountChars($input)
    {
        $l = mb_strlen($input, 'UTF-8');
        $unique = array();
        for ($i = 0; $i < $l; $i++) {
            $char = mb_substr($input, $i, 1, 'UTF-8');
            if (!array_key_exists($char, $unique)) {
                $unique[$char] = 0;
            }
            $unique[$char]++;
        }
        return $unique;
    }

    public static function getFileContent($path)
    {
        $path = \Treewec\FileSystem\Path::toFileSystem($path);
        $ext = \Treewec\FileSystem\FileType::getType($path) == 'FILE' ? '.php' : 'index.php';
        $filePath = TREEWEC_WEB_VIEW . $path . $ext;
        return file_get_contents($filePath);
    }

    public static function getSourcesPrinter()
    {
        $list = ListFacade::$NUMBER_BASIC;
        $list->setDataSource(new Sources\SourcesPrinter());
        $path = \Treewec\FileSystem\Path::toFileSystem(TREETYPE_NAME_OF_SOURCES_DIR);
        $buildArray = array(
            // directory iterator settings
            'path' => TREEWEC_WEB_VIEW . $path,
            'searchType' => \Treewec\Iterators\Directory\DirectoryIteratorType::BASIC,
            'filter' => 'default',
            'comparator' => 'default',
            'hiddenFiles' => true,
            // processing setttings
            'processRoot' => false,
            'callback' => $list,
        );
        return new \Treewec\Iterators\IteratorFacade($buildArray);
    }

    public static function getIterator($printer, $startRoot = false)
    {
        $path = '';
        if ($startRoot == false) {
            $currentPath = \Treewec\TreewecPage::getPageInfo()->currentPath;
            $path = \Treewec\FileSystem\Path::toFileSystem($currentPath);
        }
        $buildArray = array(
            // directory iterator settings
            'path' => TREEWEC_WEB_VIEW . $path,
            'searchType' => \Treewec\Iterators\Directory\DirectoryIteratorType::DEPTH_FIRST,
            'filter' => 'default',
            'comparator' => 'default',
            'hiddenFiles' => false,
            'depth' => 10,
            // processing setttings
            'processRoot' => $startRoot,
            'callback' => $printer,
        );
        return new \Treewec\Iterators\IteratorFacade($buildArray);
    }
}

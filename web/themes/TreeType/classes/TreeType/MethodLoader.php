<?php
/*
 * TreeType (https://bitbucket.org/treewec/treetype)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace TreeType;

final class MethodLoader
{
    public static function getMethod($pageInfo)
    {
        $action = isset($_GET['action']) ? $_GET['action'] : '';
        $pages = isset($_GET['pages']) ? $_GET['pages'] : '';
        if ($pageInfo->header->header == TREEWEC_HEADER_ERROR) {
            return 'error';
        } elseif ($action == 'indexes') {
            return $pages == 'all' ? 'indexesDocument' : 'indexesPage';
        } elseif ($action == 'analysis') {
            return $pages == 'all' ? 'analysisDocument' : 'analysisPage';
        } elseif ($action == 'todo') {
            return 'todo';
        } elseif ($pageInfo->pageType == \Treewec\FileSystem\FileType::FOLDER) {
            return $pages == 'all' ? 'directoryAllInOne' : 'directory';
        } elseif ($pageInfo->pageType == \Treewec\FileSystem\FileType::FILE) {
            $isSource = is_int(strpos($pageInfo->currentPath, TREETYPE_NAME_OF_SOURCES_DIR));
            return $isSource ? 'source' : 'file';
        } else {
            return 'error';
        }
    }
}

<?php 
use TreeType\PageInfo\PageInfoPrinter;
use TreeType\PageInfo\PageInfoUpdater;
require_once(TREEWEC_WEB_CONFIG . 'treetype.php');
PageInfoUpdater::updateHeaders($pageInfo); 
$controller = new \TreeType\Controller($pageInfo, $view);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <base href="<?php echo TREEWEC_WEB_URL; ?>" />
        <title><?php echo \TreeType\Elements\Title::getTitle($pageInfo); ?></title>
        <meta name="robots" content="index, follow">
        <link href="<?php echo TREEWEC_THEME_PUBLIC_URL; ?>css/screen.css" type="text/css" rel="stylesheet" media="all" />
        <link href="<?php echo TREEWEC_THEME_PUBLIC_URL; ?>css/print.css" type="text/css" rel="stylesheet" media="print" />
        <script src="<?php echo TREEWEC_THEME_PUBLIC_URL; ?>jquery/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo TREEWEC_THEME_PUBLIC_URL; ?>jquery/sources.js" type="text/javascript"></script>
<?php include(__DIR__ . '/helpers/css-counters.php'); ?>
        <script type="text/javascript">
        $(document).ready(function() {new Sources(<?php \TreeType\Sources\SourcesHelper::printJavascriptArray(); ?>, <?php \TreeType\Sources\SourcesHelper::printIfHideNotUsedLinks($pageInfo); ?>);});
        </script>
<?php
if (defined('TREEWEC_ADMIN')) {
    
    $defaultStyles = json_decode(file_get_contents(TREEWEC_THEME_PUBLIC_URL . 'defaultStyles.json'));
    foreach (TreeType\Sources\SourcesHelper::getStylesSource() as $class => $name) {
        foreach (TreeType\Sources\SourcesHelper::getStylesCKEditorElements() as $element) {
            $style = new stdClass();
            $style->name = "Zdroj - {$name}";
            $style->element = $element;
            $attribute = new stdClass;
            $attribute->dataSource = $class;
            $style->attributes = $attribute;
            $defaultStyles->styles[] = $style;
        }
    }
    $jsonDecode = json_encode($defaultStyles->styles);
    $jsonDecode = str_replace('dataSource', 'data-source', $jsonDecode);
    
    $adminUrl = TREEWEC_THEME_PUBLIC_URL;
    echo <<<STYLE
            <script type="text/javascript">
                var ckEditorthemeStyles = {$jsonDecode};
            </script>
            <link href="{$adminUrl}css/admin.css" type="text/css" rel="stylesheet" />
STYLE;
}
?> 
    </head>
    <body>     
        
        <header id="top">
            <nav>
                <?php include(__DIR__ . '/helpers/menu.php'); ?>
            </nav>
        </header>
        
        <div id="main">
            <nav>
                <?php include(__DIR__ . '/helpers/neighbours.php'); ?>
            </nav>
            
            <div>
                <nav><?php PageInfoPrinter::printBreadcrumbNavigation($pageInfo, ' &raquo; '); ?></nav>

                <?php
                call_user_func(array($controller, \TreeType\MethodLoader::getMethod($pageInfo)));
                $controller->displayView();
                ?>

                
                <?php
                if (defined('TREEWEC_ADMIN')) {
                    echo <<<TXT
                        <hr />
                        <h2 class="noPrefix">Dostupné zdroje pro data-source atribut</h2>
                        <table class="adminSources">
TXT;
                    
                    foreach (TreeType\Sources\SourcesHelper::getStylesSource() as $class => $name) {
                        $path = Treewec\FileSystem\Path::toFileSystem(TREETYPE_NAME_OF_SOURCES_DIR . "/{$class}.php");                        
                        $content = file_get_contents(TREEWEC_WEB_VIEW . $path);
                        echo <<<ROW
                            <tr>
                                <td class="cssClass"><strong>{$class}</strong></td>
                                <td>{$content}</td>
                            </tr>
ROW;
                    }
                    echo '</table>';
                }
                ?>
                
            </div>
        </div>
        
        <footer>
            <?php include(__DIR__ . '/helpers/neighbours.php'); ?>
            
        </footer>

        <div id="backToTop"><a href="<?php 
            $urlBuilder = \Treewec\TreewecPage::getUrlBuilder();
            echo $urlBuilder->build(); 
        ?>#top">&#9650;</a></div>             
        
    </body>
</html>
<?php
/*
 * $header
 * $basePathForUrlLink
 * $marks
 */
?>

            <?php if ($marks): ?>
            <a href="<?php echo $basePathForUrlLink; ?>"><strong><?php echo $header; ?></strong></a> (<em><?php echo $basePathForUrlLink; ?></em>)
                <ul>
                <?php foreach ($marks as $mark): ?>
                <li><?php echo $mark; ?></li>
                <?php endforeach; ?>
            </ul>
            <?php endif; ?>

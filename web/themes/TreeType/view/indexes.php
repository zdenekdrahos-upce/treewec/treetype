<?php 
/**
 * $illustrations
 * $tables
 * $abbrevations
 */
?>                

                <h2 class="noPrefix">Seznam obrázků</h2>
                <ul class="index">
                <?php foreach ($illustrations as $number => $illustration): ?>
                    <li>
                        <span><a href="<?php echo $illustration->getPath(); ?>" title="<?php echo $illustration->getPath(); ?>">Obrázek <?php echo ($number + 1) . "</a> &ndash; {$illustration}"; ?></span>
                        <figure><?php echo $illustration->getContent(); ?></figure>
                    </li>
                <?php endforeach; ?>
                </ul>
                
                <h2 class="noPrefix">Seznam tabulek</h2>
                <ul class="index">
                <?php foreach ($tables as $number => $table): ?>                
                    <li>
                        <span><a href="<?php echo $table->getPath(); ?>" title="<?php echo $table->getPath(); ?>">Tabulka <?php echo ($number + 1) . "</a> &ndash; {$illustration}"; ?></span>
                        <figure><?php echo $table->getContent(); ?></figure>
                    </li>
                <?php endforeach; ?>
                </ul>
                
                <h2 class="noPrefix">Seznam zkratek</h2>
                <dl>
                <?php foreach ($abbrevations as $abbrevation): ?>                
                    <dt><a href="<?php echo $abbrevation->getPath(); ?>" title="<?php echo $abbrevation->getPath(); ?>"><?php echo $abbrevation; ?></a></dt>
                    <dd><?php echo $abbrevation->getContent(); ?></dd>
                <?php endforeach; ?>
                </dl>


<?php 
/** 
 * $tag array('start' => ..., 'end' => ...)
 * $id 
 * $basePathForUrlLink
 * $headerLink
 * $includedPath
 */
?>

                <<?php echo $tag['start']; ?> id="<?php echo $id; ?>">
                    <a href="<?php echo $basePathForUrlLink; ?>"><?php echo $header; ?></a>
                    <a href="<?php echo "admin/{$basePathForUrlLink}"; ?>"><img src="<?php echo TREEWEC_THEME_PUBLIC_URL; ?>images/edit.png" height="15px" /></a>
                </<?php echo $tag['end']; ?>>

                <?php 
                include($includedPath); 
                include(__DIR__ . '/notes.php');
                TreeType\Elements\Links::resetNotes();
                ?>

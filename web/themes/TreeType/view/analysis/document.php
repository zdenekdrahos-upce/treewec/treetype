<?php
/*
 * $this    AnalyzerPrinter
 */
?>

        <?php if($this->pageCount == 0): ?>
        <table id="wordUsage">
            <tr>
                <th>Stránka</th>
                <th>Počet slov</th>
                <th>Počet znaků</th>
                <th>Počet znaků bez mezer</th>
            </tr>
       <?php elseif (isset($analyzer)): ?>
            <tr>
                <td><a href="<?php echo $basePathForUrlLink; ?>"><?php echo $header; ?><a></td>
                <td><?php echo $analyzer->getWordCount(); ?></td>
                <td><?php echo $analyzer->getAllCharCount(); ?></td>
                <td><?php echo $analyzer->getCharCount(); ?></td>
            </tr>            
       <?php else: ?>
            <tr>
                <th>Celkem</th>
                <th><?php echo $this->wordCount; ?></th>
                <th><?php echo $this->allCharCount; ?></th>
                <th><?php echo $this->nonSpaceCharCount; ?></th>
            </tr>
            <tr>
                <th>Průměry</th>
                <th><?php echo \TreeType\Stats::getShare($this->wordCount, $this->pageCount); ?></th>
                <th><?php echo \TreeType\Stats::getShare($this->allCharCount, $this->pageCount); ?></th>
                <th><?php echo \TreeType\Stats::getShare($this->nonSpaceCharCount, $this->pageCount); ?></th>
            </tr>
        </table>
        <?php endif; ?>
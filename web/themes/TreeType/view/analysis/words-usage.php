<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

        <?php if($this->pageCount == 0): ?>
        <table id="wordUsage">
            <tr>
                <th>Stránka</th>
                <th>Počet (Case Sensitive)</th>
                <th>Počet (case insensitive)</th>
            </tr>
       <?php elseif (isset($analyzer)): ?>
            <tr>
                <td><a href="<?php echo $basePathForUrlLink; ?>"><?php echo $header; ?><a></td>
                <td><?php echo $caseSensitive; ?></td>
                <td><?php echo $caseInsensitive; ?></td>
            </tr>    
       <?php else: ?>
            <tr>
                <th>Celkem</th>
                <th><?php echo $this->wordsCount['caseSensitive']; ?></th>
                <th><?php echo $this->wordsCount['caseInsensitive']; ?></th>
            </tr>
            <tr>
                <th>Průměry</th>
                <th><?php echo \TreeType\Stats::getShare($this->wordsCount['caseSensitive'], $this->pageCount); ?></th>
                <th><?php echo \TreeType\Stats::getShare($this->wordsCount['caseInsensitive'], $this->pageCount); ?></th>
            </tr>
        </table>
        <?php endif; ?>
<?php 
/**
 * $analyzer
 * $words
 */
?>

                <ul>
                    <li>Počet slov: <strong><?php echo $analyzer->getWordCount(); ?></strong></li>
                    <li>Celkem znaků: <strong><?php echo $analyzer->getAllCharCount(); ?></strong></li>
                    <li>Počet znaků bez mezer: <strong><?php echo $analyzer->getCharCount(); ?></strong></li>
                </ul>
                
                <h2 class="noPrefix">Počet výskytů hledaných slov</h2>
                <?php include(__DIR__ . '/search-form.php'); ?>

                <?php if ($words): ?>
                <table id="wordUsage">
                    <tr>
                        <th>Slovo</th>
                        <th>Počet (Case Sensitive)</th>
                        <th>Počet (case insensitive)</th>
                    </tr>
                    <?php
                    foreach ($words as $searchedWord):
                    $usage = $analyzer->getWordUsage($searchedWord);
                    ?>
                    <tr>
                        <td><strong><?php echo $searchedWord; ?></strong></td>
                        <td><?php echo $usage['caseSensitive']; ?></td>
                        <td><?php echo $usage['caseInsensitive']; ?></td>
                    </tr>
                    <?php endforeach; ?>
                </table>
                <?php endif; ?>


                <h2 class="noPrefix">Počet výskytů jednotlivých znaků</h2>
                <table id="charAnalysis">
                    <?php 
                    $max = $analyzer->getCharCountArray() ? max($analyzer->getCharCountArray()) : 0;
                    foreach($analyzer->getCharCountArray() as $char => $count): 
                    ?>
                    <tr>
                        <th><?php echo $char; ?></th>
                        <td class="count<?php echo $count == $max ? " max" : ''; ?>"><?php echo $count; ?></td>
                        <td class="graph"><span style="width: <?php echo ceil(100 * $count / $max); ?>%">&nbsp;</span></td>
                    </tr>
                    <?php endforeach; ?>
                </table>
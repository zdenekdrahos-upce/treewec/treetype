<?php 
/**
 * $cssClass
 */
?>

                <br /><br />
                <h2 class="noPrefix">Informace o použití zdroje</h2>
                <p>Obsah souboru se vypíše jako zdroj. Pro vložení zdroje do textu se použije atribut data-source, která 
                    vznikne <strong>jméno-souboru</strong>, kde jméno souboru je malými písmeny a 
                    mezery nahrazuje spojovník (-). Třídu můžete připojit k libovolnému elementu (odstavec, 
                    popisek obrázku &hellip;). </p>
                <p>Pro přidání informace o zdroji do textu použijte u elementu CSS atribut <strong>data-source</strong> 
                    <strong><?php echo $cssClass ; ?></strong>. V contentEditable editoru
                    bude třídy automaticky nabídnuta jako ve stylech objektu. V CKEditoru jde
                    doplnit styl k následujícím tagům:
                </p>
                <code><?php echo implode(', ', TreeType\Sources\SourcesHelper::getStylesCKEditorElements()); ?></code>
                <p>K ostatním objektům musí bohužel styl doplnit ručně.</p>
                
                <h2 class="noPrefix">Zdroj se používá na stránkách</h2>
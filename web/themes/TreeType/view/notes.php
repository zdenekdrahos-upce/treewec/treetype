
                <?php if(TreeType\Elements\Links::existsNotes()): ?>
                <aside class="notes">
                    <ol>
                    <?php foreach (TreeType\Elements\Links::getNotes() as $id => $text): ?>
                        <li id="<?php echo $id; ?>"><?php echo $text; ?></li>
                    <?php endforeach; ?>
                    </ol>
                </aside>
                <?php endif; ?>

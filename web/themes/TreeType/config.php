<?php
/** 
 * Configuration of theme 
 * - absoluteUrlToPublicFolder
 * - filePathToPublicFolder
 * - ckeditorStylesJs
 * [optional autoloadCallback - autoload callback in classes/autoload.php]
 */

require_once(__DIR__ . '/classes/autoload.php');

$themeConfig = array(
    'absoluteUrlToPublicFolder' => TREEWEC_WEB_URL . 'public/themes/TreeType/',
    'filePathToPublicFolder' => TREEWEC_SITE_ROOT . 'public/themes/TreeType/',
    'ckeditorStylesJs' => TREEWEC_WEB_URL . 'public/themes/TreeType/styles.js',
    'autoloadCallback' => 'treeTypeAutoload'
);
?>
<?php

namespace TreeType\Typography\Action;

/**
 * Test class for EntityConvertor.
 * Generated by PHPUnit on 2012-09-11 at 10:13:32.
 * @group typography
 */
class EntityConvertorTest extends \PHPUnit_Framework_TestCase
{
    /** @var EntityConvertor */
    private $entityToCharConvertor;
    /** @var EntityConvertor */
    private $charToEntityConvertor;

    protected function setUp()
    {
        $this->entityToCharConvertor = new EntityConvertor(EntityConvertor::HTML_ENTITY_TO_CHAR);
        $this->charToEntityConvertor = new EntityConvertor(EntityConvertor::CHAR_TO_HTML_ENTITY);
    }

    public function testModify()
    {
        parent::assertEquals(
                $this->getTextWithUnicodeChars(),
                $this->entityToCharConvertor->modify($this->getTextWithHTMLEntities())
        );
        parent::assertEquals(
                $this->getTextWithHTMLEntities(),
                $this->charToEntityConvertor->modify($this->getTextWithUnicodeChars())
        );
    }

    private function getTextWithUnicodeChars()
    {
        return <<<TEXT
        Hello World.
        Hello World…
        5 − 3 = 2
        czech-english can be breaked
        english‑czech cannot be breaked
        Do you know difference between – and —?
        Go this way: ← ← ↓ → ↑
        5 < 20 > 5
TEXT;
    }

    private function getTextWithHTMLEntities()
    {
        return <<<TEXT
        Hello World.
        Hello&nbsp;World&hellip;
        5 &minus; 3 = 2
        czech-english can be breaked
        english&#8209;czech cannot be breaked
        Do you know difference between &ndash; and &mdash;?
        Go this way: &larr; &larr; &darr; &rarr; &uarr;
        5 &lt; 20 &gt; 5
TEXT;
    }
}

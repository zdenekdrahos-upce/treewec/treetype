<?php
define('TREETYPE_SITE_ROOT', __DIR__ . '/../../');
$webConfig = array(
    'webDirectory' =>  TREETYPE_SITE_ROOT . '/web/',
    'viewDirectory' =>  TREETYPE_SITE_ROOT . '/web/view/',
    'registerDieAutoload' => false,
    'displayUncaughtExceptions' => true,
);
require_once(TREETYPE_SITE_ROOT . "/../../_latest/treewec/bootstrap.php");

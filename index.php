<?php
$webConfig = array(
    'webDirectory' =>  __DIR__ . '/web/',
    'viewDirectory' =>  __DIR__ . '/web/view/',
    'registerDieAutoload' => false,
    'displayUncaughtExceptions' => false,
);
require_once(__DIR__ . "/../../_latest/treewec/bootstrap.php");
$treewec = new Treewec\Treewec();
$treewec->run();
?>